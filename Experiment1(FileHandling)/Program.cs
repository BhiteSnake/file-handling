﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling1
{
    class Program
    {
        static void Main(string[] args)
        {
            string N = "Cardo Dalisay";
            string S = "SD 101AL JAVA Programming";
            string C = "MW 3:00PM - 6:00PM";
            string L = "Girls";
            string D = "Gays";
            string H = "Eating while Sleeping";

            string w = "Write";
            string r = "Read";
            string u = "Update";
            string d = "Delete";
            string v = "View";

            Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
            Console.WriteLine(w);
            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
            Console.WriteLine(r);
            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
            Console.WriteLine(u);
            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
            Console.WriteLine(d);
            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
            Console.WriteLine(v);


            string n = "Name";
            string s = "Subject";
            string c = "sChedule";
            string l = "Likes";
            string k = "Dislikes";
            string h = "Hobbies";

            var key = Console.ReadKey(true).Key;

            do
            {   
                if (key == ConsoleKey.W)
                {
                    Console.Clear();
                    //creating text file
                    FileStream cr = new FileStream("E:\\SD104AL.txt", FileMode.Create);
                    cr.Close();

                    //writing on the text file
                    string[] lines = { "Name: " + N, "Subject: " + S, "Schedule: " + C, "Likes: " + L, "Dislikes: " + D, "Hobbies: " + H };
                    System.IO.File.WriteAllLines(@"E:\\SD104AL.txt", lines);

                    string notf = "TextFile Succesfully Created! Press ENTER to continue!";
                    Console.SetCursorPosition((Console.WindowWidth) / 4, Console.WindowHeight / 2);
                    Console.WriteLine(notf);

                }
                else if (key == ConsoleKey.R)
                {
                    if (File.Exists(@"E:\\SD104AL.txt"))
                    {
                       Console.Clear();
                       string[] lines = System.IO.File.ReadAllLines(@"E:\\SD104AL.txt");
                       string nam = lines[0];
                       string sub = lines[1];
                       string sch = lines[2];
                       string lik = lines[3];
                       string dis = lines[4];
                       string hob = lines[5];

                       Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 3);
                       Console.WriteLine(nam);
                       Console.SetCursorPosition((Console.WindowWidth) / 3, Console.CursorTop);
                       Console.WriteLine(sub);
                       Console.SetCursorPosition((Console.WindowWidth) / 3, Console.CursorTop);
                       Console.WriteLine(sch);
                       Console.SetCursorPosition((Console.WindowWidth) / 3, Console.CursorTop);
                       Console.WriteLine(lik);
                       Console.SetCursorPosition((Console.WindowWidth) / 3, Console.CursorTop);
                       Console.WriteLine(dis);
                       Console.SetCursorPosition((Console.WindowWidth) / 3, Console.CursorTop);
                       Console.WriteLine(hob);                
                    }
                    else
                    {
                        Console.Clear();
                        string no = "ERROR! TextFile does not exist! Press ENTER to continue!";
                        Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                        Console.Write(no);
                    }
                    key = Console.ReadKey(true).Key;
                    Console.Clear();
                    Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                    Console.WriteLine(w);
                    Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                    Console.WriteLine(r);
                    Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                    Console.WriteLine(u);
                    Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                    Console.WriteLine(d);
                    Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                    Console.WriteLine(v);                
                }
                else if (key == ConsoleKey.U)
                {
                    if (File.Exists(@"E:\\SD104AL.txt"))
                    {
                        Console.Clear();
                        Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                        Console.WriteLine(n);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(s);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(c);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(l);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(k);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(h);

                        do
                        {

                            if (key == ConsoleKey.N)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: ", n);
                                N = Console.ReadLine();
                            }
                            else if (key == ConsoleKey.S)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: ", s);
                                S = Console.ReadLine();
                            }
                            else if (key == ConsoleKey.C)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: ", c);
                                C = Console.ReadLine();
                            }
                            else if (key == ConsoleKey.L)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: ", l);
                                L = Console.ReadLine();
                            }
                            else if (key == ConsoleKey.D)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: ", k);
                                D = Console.ReadLine();
                            }
                            else if (key == ConsoleKey.H)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: ", h);
                                H = Console.ReadLine();

                            }
                            Console.Clear();
                            Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                            Console.WriteLine(n);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(s);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(c);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(l);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(k);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(h);
                            string esc = "ESCAPE";
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.Write(esc);

                            key = Console.ReadKey(true).Key;
                            Console.Clear();
                            string[] lines = { "Name: " + N, "Subject: " + S, "Schedule: " + C, "Likes: " + L, "Dislikes: " + D, "Hobbies: " + H };
                            System.IO.File.WriteAllLines(@"E:\\SD104AL.txt", lines);

                        } while (key != ConsoleKey.Escape);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                        Console.WriteLine(w);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(r);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(u);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(d);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(v);

                    }
                    else
                    {
                        Console.Clear();
                        string no = "ERROR! TextFile does not exist! Press ENTER!";
                        Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                        Console.Write(no);
                    }

                
                }
                else if (key == ConsoleKey.V)
                {
                    if (File.Exists(@"E:\\SD104AL.txt"))
                    {
                        Console.Clear();
                        Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                        Console.WriteLine(n);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(s);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(c);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(l);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(k);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(h);

                        do
                        {

                            if (key == ConsoleKey.N)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: {1}", n, N);
                                key = Console.ReadKey(true).Key;
                            }
                            else if (key == ConsoleKey.S)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: {1}", s, S);
                                key = Console.ReadKey(true).Key;
                            }
                            else if (key == ConsoleKey.C)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: {1}", c, C);
                                key = Console.ReadKey(true).Key;
                            }
                            else if (key == ConsoleKey.L)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: {1}", l, L);
                                key = Console.ReadKey(true).Key;
                            }
                            else if (key == ConsoleKey.D)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: {1}", k, D);
                                key = Console.ReadKey(true).Key;
                            }
                            else if (key == ConsoleKey.H)
                            {
                                Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                                Console.Write("{0}: {1}", h, H);
                                key = Console.ReadKey(true).Key;
                            }

                            Console.Clear();
                            Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                            Console.WriteLine(n);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(s);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(c);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(l);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(k);
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.WriteLine(h);
                            string esc = "ESCAPE";
                            Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                            Console.Write(esc);

                            key = Console.ReadKey(true).Key;
                            Console.Clear();
                            string[] lines = { "Name: " + N, "Subject: " + S, "Schedule: " + C, "Likes: " + L, "Dislikes: " + D, "Hobbies: " + H };
                            System.IO.File.WriteAllLines(@"E:\\SD104AL.txt", lines);

                        } while (key != ConsoleKey.Escape);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                        Console.WriteLine(w);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(r);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(u);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(d);
                        Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                        Console.WriteLine(v);

                        
                    }
                    else
                    {
                        Console.Clear();
                        string no = "ERROR! TextFile does not exist! Press ENTER!";
                        Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                        Console.Write(no);
                    }
                }
                else if (key == ConsoleKey.D)
                {
                    if (File.Exists(@"E:\\SD104AL.txt"))
                    {
                        Console.Clear();
                        File.Delete("E:\\SD104AL.txt");
                        string del = "TextFile is successfully Deleted! Press ENTER!";
                        Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                        Console.Write(del);
                    }
                    else
                    {
                        Console.Clear();
                        string no = "ERROR! TextFile does not exist! Press ENTER!";
                        Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
                        Console.Write(no);
                    }
                }
                key = Console.ReadKey(true).Key;
                Console.Clear();
                Console.SetCursorPosition((Console.WindowWidth) / 2, (Console.WindowHeight) / 3);
                Console.WriteLine(w);
                Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                Console.WriteLine(r);
                Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                Console.WriteLine(u);
                Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                Console.WriteLine(d);
                Console.SetCursorPosition((Console.WindowWidth) / 2, Console.CursorTop);
                Console.WriteLine(v);
            } while (key != ConsoleKey.Escape);
            Console.Clear();
            string end = "Press ENTER to EXIT!";
            Console.SetCursorPosition((Console.WindowWidth) / 3, (Console.WindowHeight) / 2);
            Console.Write(end);

            Console.Read();
        }
    }
}
